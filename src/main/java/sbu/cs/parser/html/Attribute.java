package sbu.cs.parser.html;

/**
 * A class for HTML node attributes
 */
public class Attribute {
    private String key;
    private String value;
    /**
     * Contracting an attribute
     * @param key The option
     * @param value the value
     */
    public Attribute(String key, String value){
        this.key = key;
        this.value = value;
    }

    /**
     * Get the attribute key
     * @return the attribute key
     */
    public String getKey(){
        return key;
    }
    /**
     * Get the attribute value
     * @return the attribute value
     */
    public String getValue(){
        return value;
    }
}
