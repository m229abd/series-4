package sbu.cs.parser.html;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTMLParser {

    /*
    * this function will get a String and returns a Dom object
     */
    public static Node parse(String document) {
        Node node = null;
        //Find the tags:
        Pattern tagPattern = Pattern.compile("<[\\w\\d/]+");
        Matcher tagFinder = tagPattern.matcher(document);
        if(tagFinder.find()){
            //If the outer tag has children:
            if(tagFinder.group(0).charAt(1) != '/') {
                //If it doesn't have attributes:
                if(document.charAt(tagFinder.end(0)) != ' ') {
                    String inside = document.substring(tagFinder.end(0) + 2, document.length() - (tagFinder.group(0).length() + 2));
                    node = new Node(tagFinder.group(0), inside, null);
                }
                //If it has attributes:
                else{
                    String inside = document.substring(document.indexOf('>') + 1, document.length() - (tagFinder.group(0).length() + 2));
                    String tag = document.substring(tagFinder.start(0),document.indexOf('>') + 1);
                    node = new Node(tag.substring(0,tagFinder.group(0).length()), inside, tag.substring(tagFinder.group(0).length() + 1,tag.length() - 1));
                }
            }
            //If the outer tag doesn't have children:
            else{
                //If it doesn't have attributes:
                if(document.charAt(tagFinder.end(0) + 1) != ' ') {
                    node = new Node(tagFinder.group(0) + ">", null, null);
                }
                //If it has attributes:
                else{
                    String tag = document.substring(tagFinder.start(0),document.indexOf('>') + 1);
                    node = new Node(tag, null, tag.substring(tagFinder.group(0).length(),tag.length() - 1));
                }
            }
        }
        return node;
    }

    /*
    * a function that will return string representation of dom object.
    * only implement this after all other functions been implemented because this
    * impl is not required for this series of exercises. this is for more score
     */
    public static String toHTMLString(Node root) {
        //Build the output string:
        StringBuilder output = new StringBuilder();
        //If the parent tag doesn't have children:
        if(root.getParentTag().charAt(1) != '/'){
            output.append(root.getParentTag());
            //If the parent tag has attributes add the to the string:
            if(root.getAttributeString() != null)
                output.append(" " + root.getAttributeString() + ">");
            //If it doesn't have attributes:
            else
                output.append(">" + "\n");
            //Add the inside value:
            output.append(root.getStringInside());
            //Close the tag:
            output.append(root.getParentTag() + ">");
            output.insert(output.length()-(root.getParentTag().length()),'/');
        }
        //If the parent only has children:
        else{
            output.append(root.getParentTag());
            //If the parent tag has attributes add the to the string:
            if(root.getAttributeString() != null)
                output.append(" " + root.getAttributeString() + ">");
            else
            //If it doesn't have attributes:
                output.append(">" + "\n");
        }
        return output.toString();
    }
}
