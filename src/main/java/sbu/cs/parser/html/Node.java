package sbu.cs.parser.html;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Node implements NodeInterface {
    //Each Node should have the following attributes:
    private String inside;
    private String attributesString;
    private String parentTag;
    List<Node> children;
    List<Attribute> attributes;

    /**
     * Contracting a node object
     * @param parentTag The tag that contains the children
     * @param inside A string of inner tags and values
     * @param attributesString A string of main tag attribute
     */
    public Node(String parentTag,String inside,String attributesString){
        this.parentTag = parentTag;
        this.inside = inside;
        this.attributesString = attributesString;
    }
    /*
    * this function will return all that exists inside a tag
    * for example for <html><body><p>hi</p></body></html>, if we are on
    * html tag this function will return <body><p1>hi</p1></body> and if we are on
    * body tag this function will return <p1>hi</p1> and if we are on
    * p tag this function will return hi
    * if there is nothing inside tag then null will be returned
     */
    @Override
    public String getStringInside() {
        return inside;
    }

    /**
     * @return The attributes for the parent tag
     */

    public String getAttributeString(){
        return attributesString;
    }

    @Override
    /**
     * @return A list of children in the parent tag
     */
    public List<Node> getChildren() {
        if(children == null)
            extractChildren();
        return children;
    }
    /**
     * Extract children in a more efficient manner
     */
    private void extractChildren() {
        //Create a list of values:
        children = new ArrayList<>();
        //Search for tags:
        int searchIndex = 0;
        Pattern tagPattern = Pattern.compile("<[\\w\\d/]+");
        Matcher tagFinder = tagPattern.matcher(inside);
        while(tagFinder.find()){
            //If a new tag is found:
            int childStartIndex = searchIndex + tagFinder.start(0) + tagFinder.group(0).length() + 1;
            //If the tag has children but no attributes:
            if(inside.charAt(childStartIndex - 1) == '>' && tagFinder.group(0).charAt(1) != '/'){
                //Construct the ending tag:
                String childEndTag = "</" + tagFinder.group(0).substring(1) + ">";
                //Find the ending tag index:
                int childEndIndex = childStartIndex + inside.substring(childStartIndex).indexOf(childEndTag);
                //Extract the children string:
                String child = inside.substring(childStartIndex,childEndIndex);
                //Create a node for the found tag:
                children.add(new Node(tagFinder.group(0) + ">",child,null));
                //Next time start after this tag:
                searchIndex = childEndIndex + childEndTag.length();
                tagFinder = tagPattern.matcher(inside.substring(searchIndex));
            }
            //If the tag has attributes but no children:
            else if (inside.charAt(childStartIndex - 1) == ' ' && tagFinder.group(0).charAt(1) == '/'){
                //Find the ending index of the tag:
                int childEndIndex = childStartIndex + inside.substring(childStartIndex).indexOf('>');
                //Extract the attributes:
                String childAttributes = inside.substring(childStartIndex - 1,childEndIndex);
                //Create a node for the found tag:
                children.add(new Node(tagFinder.group(0) + " ",null,childAttributes));
                //Next time start after this tag:
                searchIndex = childEndIndex + 1;
                tagFinder = tagPattern.matcher(inside.substring(searchIndex));
            }
            //If the tag has both children and attributes:
            else if (inside.charAt(childStartIndex - 1) == ' ' && tagFinder.group(0).charAt(1) != '/'){
                //Find the ending index of the tag:
                int childAttributeEndIndex = childStartIndex + inside.substring(childStartIndex).indexOf('>');
                //Extract the attributes:
                String childAttributes = inside.substring(childStartIndex - 1,childAttributeEndIndex);
                //Construct the ending tag:
                String childEndTag = "</" + tagFinder.group(0).substring(1) + ">";
                //Extract the children string:
                int childInsideEndIndex = childAttributeEndIndex + inside.substring(childAttributeEndIndex).indexOf(childEndTag);
                String child = inside.substring(childAttributeEndIndex + 1,childInsideEndIndex);
                //Create a node for the found tag:
                children.add(new Node(tagFinder.group(0) + " ",child,childAttributes));
                //Next time start after this tag:
                searchIndex = childInsideEndIndex + childEndTag.length();
                tagFinder = tagPattern.matcher(inside.substring(searchIndex));
            }
            //Any other tags:
            else
                //Create a null node for the found tag:
                children.add(new Node(tagFinder.group(0) + ">", null, null));
        }
    }

    /*
    * in html tags all attributes are in key value shape. this function will get a attribute key
    * and return it's value as String.
    * for example <img src="img.png" width="400" height="500">
     */
    @Override
    /**
     * Gets the value for an option in the tag
     * @param key The key
     * @return The value for the option
     */
    public String getAttributeValue(String key) {
        if(attributes == null)
            extractAttributes();
        //Check if the key exists in any attributes:
        for(Attribute attribute: attributes)
            if(attribute.getKey().equals(key))
                return attribute.getValue();
        //If not return null:
        return null;
    }

    /**
     * Extract attributes in a more efficient manner
     */
    private void extractAttributes() {
        attributes = new ArrayList<>();
        //Find the keys for attributes:
        Pattern keyPattern = Pattern.compile(" [\\w\\d]+=\"");
        Matcher keyFinder = keyPattern.matcher(attributesString);
        //For each found key:
        while(keyFinder.find()){
            //Extract the key:
            String attributeKey = keyFinder.group().substring(1,keyFinder.group().length() - 2);
            //Find the ending index for the value:
            int attributeEndIndex = keyFinder.end() + attributesString.substring(keyFinder.end()).indexOf('"');
            //Extract the value:
            String attributeValue = attributesString.substring(keyFinder.end(),attributeEndIndex);
            //Add the value to the list:
            attributes.add(new Attribute(attributeKey,attributeValue));
        }
    }

    /**
     * @return The parent tag
     */
    public String getParentTag(){
        return parentTag;
    }
}
