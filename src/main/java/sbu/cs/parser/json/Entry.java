package sbu.cs.parser.json;

/**
 * A class for managing json object entries
 */
public class Entry {
    //Defining entery parts:
    private String key;
    private String value;

    /**
     * Contracting an entry object
     * @param key The key
     * @param value Corresponding value
     */
    public Entry(String key, String value){
        this.key = key;
        this.value = value;
    }

    /**
     * Getting access to key
     * @return the key
     */
    public String getKey(){
        return key;
    }
    /**
     * Getting access to value
     * @return the value for key
     */
    public String getValue(){
        return value;
    }
}
