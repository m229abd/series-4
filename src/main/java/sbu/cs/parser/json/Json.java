package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.List;

public class Json implements JsonInterface {
    //A json is a list of entries:
    private List <Entry> entries;
    public Json(){
        entries = new ArrayList<>();
    }

    /**
     * Add an entry
     * @param key The entry key
     * @param value the corresponding value
     */
    public void add(String key,String value){
        entries.add(new Entry(key, value));
    }
    @Override
    /**
     * Get the value for a given key
     * @param key The given key
     * @return the value for the given key
     */
    public String getStringValue(String key) {
        for(Entry current: entries)
            if(current.getKey().equals(key))
                return current.getValue();
        return null;
    }

    /**
     * List all the entries
     * @return An array of the existing keys
     */
    public Entry[] getEntries (){
        Entry[] output = new Entry[entries.size()];
        output = entries.toArray(output);
        return output;
    }
}
