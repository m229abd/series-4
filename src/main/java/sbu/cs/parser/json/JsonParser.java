package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class JsonParser {

    /*
    * this function will get a String and returns a Json object
     */

    /**
     * Parses the string to a JSON object
     * @param data JSON data in string format
     * @return JSON object
     */
    public static Json parse(String data) {
        //Creates a JSON object
        Json output = new Json();
        //Removes the starting and end of the string:
        String[] tempArray = data.split("[\\{\\}]+");
        data = "";
        for(int i = 1; i < tempArray.length; i++)
            data+=tempArray[i];
        //Extracts the keys:
        Pattern keyPattern = Pattern.compile("\"([\\wd\\s]*?)\":");
        Matcher keyMatch = keyPattern.matcher(data);
        //Adds the found keys to a list
        List<String> keys = new ArrayList<>();
        while (keyMatch.find()) {
            keys.add(keyMatch.group());
        }
        //Extracts the values:
        List<String> values = Arrays.asList(data.split("\"([\\wd\\s]*?)\":"));
        for(int i = 1; i < values.size(); i++) {
            keys.set(i - 1, keys.get(i - 1).substring(1, keys.get(i - 1).length() - 2));
            //Checks the value type and adds them to a list:
            //Checking if the value is NOT a list or string
            if (values.get(i).indexOf('"') == -1 && values.get(i).indexOf('[') == -1) {
                Pattern pattern = Pattern.compile("[\\wd]+");
                Matcher m = pattern.matcher(values.get(i));
                if(m.find())
                    values.set(i, m.group());
            //Checks if the value is a string
            } else if ((values.get(i).indexOf('"') > -1 && values.get(i).indexOf('[') == -1)
                    || (values.get(i).indexOf('"') < values.get(i).indexOf('[') && values.get(i).indexOf('"') != -1)) {
                values.set(i, values.get(i).substring(values.get(i).indexOf('"') + 1, values.get(i).lastIndexOf('"')));
            //Any other type:
            } else {
                values.set(i, values.get(i).substring(values.get(i).indexOf('['), values.get(i).lastIndexOf(']') + 1));
            }
            //Add the key and corresponding value to a list as an entry
            output.add(keys.get(i - 1), values.get(i));
        }
        return output;
    }

    /*
    * this function is the opposite of above function. implementing this has no score and
    * is only for those who want to practice more.
     */
    public static String toJsonString(Json data) {
        //Builds a string output
        StringBuilder output = new StringBuilder();
        //Adds the starting part:
        output.append("{");
        //A checker for numeric values:
        Pattern numPattern = Pattern.compile("[\\w\\d]+");
        //Add the entries to the json string
        for(Entry entry : data.getEntries()){
            //Add the keys:
            output.append("\"");
            output.append(entry.getKey());
            output.append("\": ");
            Matcher numFinder = numPattern.matcher(entry.getValue());
            //Check if the value needs to be converted to string or not
            if(entry.getValue().equals("null")
            || entry.getValue().equals("true")
            || entry.getValue().equals("false")
            || entry.getValue().contains("[")
            || numFinder.find()){
                //If it doesn't:
                output.append(entry.getValue());
            }
            else {
                //If it does:
                output.append("\"");
                output.append(entry.getValue());
                output.append("\"");
            }
            //Next entry:
            output.append(", ");
        }
        //End the json:
        output.delete(output.length() - 2,output.length());
        output.append("}");
        return output.toString();
    }
}
